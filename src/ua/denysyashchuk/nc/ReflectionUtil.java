package ua.denysyashchuk.nc;

import org.reflections.Reflections;
import ua.denysyashchuk.nc.sorts.AbstractSort;

import java.util.ArrayList;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * Contains static methods for getting Methods and Classes with the help of Reflections
 *
 * @author Yashchuk Denys
 * @author denys.yashchuk@gmail.com
 * @version 1.0
 */
public class ReflectionUtil {

    /**
     * Gets all methods with annotation
     *
     * @param mClass     - get method from
     * @param annotation - get methods with such annotation
     * @return
     */
    public static ArrayList<Method> getMethodsWithAnnotation(Class mClass, Class annotation) {
        ArrayList<Method> methods = new ArrayList<Method>();
        for (Method m : mClass.getMethods()) {
            if (m.isAnnotationPresent(annotation)) {
                methods.add(m);
            }
        }
        return methods;
    }

    /**
     * Gets no abstract subclasses
     *
     * @param mClass - get subclasses from
     * @return
     */
    public static ArrayList<Class> getNoAbstractSubclasses(Class mClass) {
        ArrayList<Class> out = new ArrayList<Class>();
        Reflections reflections = new Reflections();
        for (Object c : reflections.getSubTypesOf(mClass)) {
            if (!Modifier.isAbstract(((Class) c).getModifiers())) {
                out.add((Class) c);
            }
        }
        return out;
    }

}
