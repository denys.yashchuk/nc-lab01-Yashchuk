package ua.denysyashchuk.nc;

import java.util.Random;

/**
 * Contains static methods for filling arrays
 *
 * @author Yashchuk Denys
 * @author denys.yashchuk@gmail.com
 * @version 1.0
 */
public class Filler {

    /**
     * Used for generating random elements
     */
    private static Random random = new Random();

    /**
     * Generate integer array with sorted elements
     *
     * @param length of the array
     * @return integer array
     * @throws IllegalArgumentException
     */
    @FillerAnnotation
    public static int[] sorted(int length) throws IllegalArgumentException {
        if (length >= 0) {
            int[] arr = new int[length];
            for (int i = 0; i < length; i++) {
                arr[i] = i;
            }
            return arr;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Generate integer array with sorted elements,
     * but last element is random
     *
     * @param length of the array
     * @return integer array
     * @throws IllegalArgumentException
     */
    @FillerAnnotation
    public static int[] sortedLastRand(int length) throws IllegalArgumentException {
        if (length >= 0) {
            int[] arr = new int[length];
            for (int i = 0; i < length - 1; i++) {
                arr[i] = i;
            }
            arr[length - 1] = random.nextInt(length);
            return arr;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Generate integer array with inverse sorted elements
     *
     * @param length of the array
     * @return integer array
     * @throws IllegalArgumentException
     */
    @FillerAnnotation
    public static int[] inverseSorted(int length) throws IllegalArgumentException {
        if (length >= 0) {
            int[] arr = new int[length];
            length--;
            for (int i = 0; i <= length; i++) {
                arr[i] = length - i;
            }
            return arr;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Generate integer array with random elements
     *
     * @param length of the array
     * @return integer array
     * @throws IllegalArgumentException
     */
    @FillerAnnotation
    public static int[] randomSorted(int length) throws IllegalArgumentException {
        if (length >= 0) {
            int[] arr = new int[length];
            for (int i = 0; i < length; i++) {
                arr[i] = random.nextInt(length);
            }
            return arr;
        } else {
            throw new IllegalArgumentException();
        }
    }
}
