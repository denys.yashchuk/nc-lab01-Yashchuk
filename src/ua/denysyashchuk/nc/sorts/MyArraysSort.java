package ua.denysyashchuk.nc.sorts;

import java.util.Arrays;

/**
 * {@inheritDoc}
 * Used native java sort method
 *
 * @author Yashchuk Denys
 * @author denys.yashchuk@gmail.com
 * @version 1.0
 * @see java.util.Arrays
 */
public class MyArraysSort extends AbstractSort {

    public MyArraysSort(int[] arr) {
        super(arr);
    }

    @Override
    public void sort() {
        Arrays.sort(arr);
    }
}
