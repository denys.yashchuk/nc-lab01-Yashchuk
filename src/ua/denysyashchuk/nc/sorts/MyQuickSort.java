package ua.denysyashchuk.nc.sorts;

import java.util.Arrays;

/**
 * {@inheritDoc}
 * implements quick sort algorithm
 *
 * @author Yashchuk Denys
 * @author denys.yashchuk@gmail.com
 * @version 1.0
 * @see <a href="https://en.wikipedia.org/wiki/Quicksort">Quick sort</a>
 */
public class MyQuickSort extends AbstractSort {

    public MyQuickSort(int[] arr) {
        super(arr);
    }

    @Override
    public void sort() {
        if (!Arrays.equals(arr, new int[0]))
            recSort(0, arr.length - 1);
    }

    /**
     * Recursively sorts array
     *
     * @param low  - bound of array for sorting
     * @param high - bound of array for sorting
     */
    private void recSort(int low, int high) {
        int pivot = arr[(low + high) / 2];
        int i = low;
        int j = high;
        while (i <= j) {
            while (arr[i] < pivot)
                i++;
            while (arr[j] > pivot)
                j--;
            if (i <= j) {
                swap(i++, j--);
            }
        }
        if (low < j) {
            recSort(low, j);
        }
        if (i < high) {
            recSort(i, high);
        }
    }
}
