package ua.denysyashchuk.nc.sorts;

/**
 * {@inheritDoc}
 * implements selection sort algorithm
 *
 * @author Yashchuk Denys
 * @author denys.yashchuk@gmail.com
 * @version 1.0
 * @see <a href="https://en.wikipedia.org/wiki/Selection_sort">Selection sort</a>
 */
public class MySelectionSort extends AbstractSort {

    public MySelectionSort(int[] arr) {
        super(arr);
    }

    @Override
    public void sort() {
        int min;
        for (int i = 0; i < arr.length - 1; i++) {
            min = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[min]) {
                    min = j;
                }
            }
            swap(i, min);
        }
    }
}
