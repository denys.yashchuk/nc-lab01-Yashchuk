package ua.denysyashchuk.nc.sorts;

import java.util.Arrays;

/**
 * Abstract class of sorts
 *
 * @author Yashchuk Denys
 * @author denys.yashchuk@gmail.com
 * @version 1.0
 */
public abstract class AbstractSort {

    /**
     * Array for sorting
     */
    protected int[] arr;

    /**
     * Creates sorting class with copy of such array
     *
     * @param arr - if null creates new empty array
     */
    public AbstractSort(int[] arr) {
        if (arr != null)
            this.arr = Arrays.copyOf(arr, arr.length);
        else
            this.arr = new int[0];
    }

    /**
     * Sorts array
     */
    public abstract void sort();

    /**
     * Sorts array and count time for sorting
     *
     * @return time in ns
     */
    public long sortWithTime() {
        long startTime = System.nanoTime();
        sort();
        long endTime = System.nanoTime();
        return endTime - startTime;
    }

    /**
     * Swaps to elements in the array
     *
     * @param a - index of first element
     * @param b - index of second element
     */
    protected void swap(int a, int b) {
        int tmp = arr[a];
        arr[a] = arr[b];
        arr[b] = tmp;
    }

    /**
     * Sets copy of such array
     *
     * @param arr - if null creates new empty array
     */
    public void setArr(int[] arr) {
        if (arr != null)
            this.arr = Arrays.copyOf(arr, arr.length);
        else
            this.arr = new int[0];
    }

    /**
     * @return integer array
     */
    public int[] getArr() {
        return arr;
    }

}
