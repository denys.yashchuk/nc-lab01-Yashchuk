package ua.denysyashchuk.nc.sorts;

import java.util.Arrays;

/**
 * {@inheritDoc}
 * implements merge sort algorithm
 *
 * @author Yashchuk Denys
 * @author denys.yashchuk@gmail.com
 * @version 1.0
 * @see @see <a href="https://en.wikipedia.org/wiki/Merge_sort">Merge sort</a>
 */
public class MyMergeSort extends AbstractSort {
    public MyMergeSort(int[] arr) {
        super(arr);
    }

    @Override
    public void sort() {
        int[] tmp = new int[arr.length];
        if(!Arrays.equals(arr, new int[0]))
            mergeSort(tmp, 0, arr.length - 1);
    }

    /**
     * Recursively marge parts of the array
     *
     * @param tmp  - array for temporary saving
     * @param low  - bound of array for sorting
     * @param high - bound of array for sorting
     */
    private void mergeSort(int[] tmp, int low, int high) {
        if (high == low) {
            return;
        } else {
            int mid = (low + high) / 2;
            mergeSort(tmp, low, mid);
            mergeSort(tmp, mid + 1, high);
            merge(tmp, low, mid + 1, high);
        }
    }

    /**
     * Merges parts of the array
     *
     * @param tmp  - array for temporary saving
     * @param low  - bound of array for sorting
     * @param mid  - middle of the part for sorting
     * @param high - bound of array for sorting
     */
    private void merge(int[] tmp, int low, int mid, int high) {
        int j = 0;
        int tmpLow = low;
        int tmpMid = mid;
        int n = high - low + 1;

        while (tmpLow < mid && tmpMid <= high) {
            if (arr[tmpLow] < arr[tmpMid]) {
                tmp[j++] = arr[tmpLow++];
            } else {
                tmp[j++] = arr[tmpMid++];
            }
        }

        while (tmpLow < mid) {
            tmp[j++] = arr[tmpLow++];
        }

        while (tmpMid <= high) {
            tmp[j++] = arr[tmpMid++];
        }
        for (j = 0; j < n; j++) {
            arr[low + j] = tmp[j];
        }
    }
}
