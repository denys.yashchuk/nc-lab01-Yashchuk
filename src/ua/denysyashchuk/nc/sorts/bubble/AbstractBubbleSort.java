package ua.denysyashchuk.nc.sorts.bubble;

import ua.denysyashchuk.nc.sorts.AbstractSort;

/**
 * {@inheritDoc}
 * implements bubble sort algorithm
 *
 * @author Yashchuk Denys
 * @author denys.yashchuk@gmail.com
 * @version 1.0
 * @see <a href="https://en.wikipedia.org/wiki/Bubble_sort">Bubble sort</a>
 */
public abstract class AbstractBubbleSort extends AbstractSort {

    public AbstractBubbleSort(int[] arr) {
        super(arr);
    }

    @Override
    public final void sort() {
        int[] indexes = getNextIndexes(null);
        while (indexes!=null) {
            if(needSwap(indexes)){
                swap(indexes[0],indexes[1]);
            }
            indexes = getNextIndexes(indexes);
        }
    }

    /*
     * @param indexes - two indexes to compare
     * @return true if numbers must be swapped
     */
    protected abstract boolean needSwap(int[] indexes);

    /**
     * @param indexes - previous indexes
     * @return null if there is no next indexes
     */
    protected abstract int[] getNextIndexes(int[] indexes);

}
