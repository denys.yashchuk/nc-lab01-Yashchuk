package ua.denysyashchuk.nc.sorts.bubble;

/**
 * {@inheritDoc}
 *
 * @author Yashchuk Denys
 * @author denys.yashchuk@gmail.com
 * @version 1.0
 */

public class MyInverseBS extends AbstractBubbleSort {

    public MyInverseBS(int[] arr) {
        super(arr);
    }

    @Override
    protected boolean needSwap(int[] indexes) {
        return arr[indexes[0]] > arr[indexes[1]];
    }

    @Override
    protected int[] getNextIndexes(int[] indexes) {
        if (indexes == null) {
            indexes = new int[2];
            indexes[0] = 0;
            indexes[1] = arr.length - 1;
            return indexes;
        } else if (indexes[1] > indexes[0]) {
            indexes[1]--;
            return indexes;
        } else if (indexes[0] < arr.length - 1) {
            indexes[1] = arr.length-1;
            indexes[0]++;
            return indexes;
        } else
            return null;
    }

}
