package ua.denysyashchuk.nc;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.charts.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

/**
 * Used for working with excel workbook
 *
 * @author Yashchuk Denys
 * @author denys.yashchuk@gmail.com
 * @version 1.0
 */
public class ExcelTableModule {

    private XSSFWorkbook workBook;

    /**
     * Creates new excel workbook
     */
    public ExcelTableModule() {
        this.workBook = new XSSFWorkbook();
    }

    /**
     * Adds values to the row in the sheet
     * @param rowName
     * @param values
     * @param sheetName
     */
    public void addRow(String rowName, long[] values, String sheetName) {
        int i;
        XSSFSheet sheet = workBook.getSheet(sheetName);
        for (i = 0; i < sheet.getLastRowNum(); i++) {
            if (sheet.getRow(i).getCell(0).getStringCellValue().equals(rowName)) {
                break;
            }
        }
        for (int j = 1; j <= values.length; j++) {
            sheet.getRow(i).createCell(j, Cell.CELL_TYPE_NUMERIC).setCellValue(values[j - 1]);
        }
    }

    /**
     * Adds values base row to the sheet
     * @param values
     * @param sheetName
     */
    public void addBaseRow(long[] values, String sheetName) {
        addRow("BASE", values, sheetName);
    }

    /**
     * Adds names for rows in sheet
     * @param names
     * @param sheetName
     */
    public void addRowName(String[] names, String sheetName) {
        XSSFSheet sheet = workBook.getSheet(sheetName);
        sheet.createRow(0).createCell(0, Cell.CELL_TYPE_STRING).setCellValue("BASE");
        for (int i = 1; i <= names.length; i++) {
            XSSFRow row = sheet.createRow(i);
            XSSFCell cell = row.createCell(0, Cell.CELL_TYPE_STRING);
            cell.setCellValue(names[i-1]);
        }
    }

    /**
     * Creates sheet with such name
     * @param sheetName
     */
    public void createSheet(String sheetName){
        XSSFSheet sheet = workBook.getSheet(sheetName);
        if (sheet == null) {
            sheet = workBook.createSheet(sheetName);
        }
    }

    /**
     * Creates charts in the excel workbook
     */
    public void createCharts() {
        Iterator<XSSFSheet> it = workBook.iterator();
        while (it.hasNext()) {
            Sheet sheet = it.next();

            Drawing drawing = sheet.createDrawingPatriarch();
            ClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 0, 8, 15, 20);

            Chart chart = drawing.createChart(anchor);
            ChartLegend legend = chart.getOrCreateLegend();
            legend.setPosition(LegendPosition.TOP_RIGHT);

            LineChartData data = chart.getChartDataFactory().createLineChartData();

            ChartAxis bottomAxis = chart.getChartAxisFactory().createCategoryAxis(AxisPosition.BOTTOM);
            ValueAxis leftAxis = chart.getChartAxisFactory().createValueAxis(AxisPosition.LEFT);
            leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);


            LineChartSeries series;
            int numOfColumns = sheet.getRow(0).getLastCellNum();
            ChartDataSource<Number> xs = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(0, 0, 1, numOfColumns - 1));
            ChartDataSource<Number> y = null;
            for (int i = 0; i < sheet.getLastRowNum(); i++) {
                y = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(i + 1, i + 1, 1, numOfColumns - 1));
                series = data.addSeries(xs, y);
                series.setTitle(sheet.getRow(i + 1).getCell(0).getStringCellValue());
            }

            chart.plot(data, bottomAxis, leftAxis);
        }
    }

    /**
     * Writes workbook to file
     *
     * @throws IOException
     */
    public void writeToFile() throws IOException {
        File file = new File("sorts.xlsx");
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream outFile = new FileOutputStream(file);
        workBook.write(outFile);
        outFile.close();
    }

}
