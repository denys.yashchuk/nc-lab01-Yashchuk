package ua.denysyashchuk.nc;

import ua.denysyashchuk.nc.sorts.AbstractSort;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Tests sorts and writes results to the excel workbook (sorts.xlsx)
 *
 * @author Yashchuk Denys
 * @author denys.yashchuk@gmail.com
 * @version 1.0
 */
public class Analyzer {

    /**
     * Saves AbstractSort's subclasses
     */
    private static ArrayList<Class> sortClasses;

    /**
     * Used for creating excel workbook
     */
    private static ExcelTableModule ex;

    public static void main(String[] args) {
        ex = new ExcelTableModule();

        System.out.println("Welcome!!!\nGetting methods of Filler class...");
        Class filler = Filler.class;
        Class annotation = FillerAnnotation.class;
        Method[] fillers = filler.getMethods();

        System.out.println("Getting AbstractSort subclasses...");
        sortClasses = ReflectionUtil.getNoAbstractSubclasses(AbstractSort.class);

        for (Method method : ReflectionUtil.getMethodsWithAnnotation(filler, annotation)) {
            System.out.println("\nTesting sorts with " + method.getName() + "...");
            try {
                testSort(method);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Creating charts...");
        ex.createCharts();

        System.out.println("Writing results to table...");
        try {
            ex.writeToFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("\nDone!!!");
    }

    /**
     * Tests sorting methods with the arrays, which are filled with special metod
     *
     * @param method for filling array
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private static void testSort(Method method) throws NoSuchMethodException, InvocationTargetException,
            IllegalAccessException, InstantiationException {
        ex.createSheet(method.getName());

        String[] methodNames = new String[sortClasses.size()];
        for (int i = 0; i < sortClasses.size(); i++) {
            String[] splitedName = sortClasses.get(i).getName().split("\\.");
            String className = splitedName[splitedName.length - 1];
            methodNames[i] = className;
        }
        ex.addRowName(methodNames,method.getName());
        long[] base = {10,100,10000,100000,1000000};
        ex.addBaseRow(base,method.getName());
        long[][] res = new long[sortClasses.size()][5];
        for (int j = 0; j<base.length; j++) {
            int[] arr = (int[]) method.invoke(null, (int)base[j]);
            for (int k = 0; k < sortClasses.size(); k++) {
                AbstractSort as = (AbstractSort) sortClasses.get(k).getConstructor(int[].class).newInstance(arr);
                res[k][j] = as.sortWithTime();
            }
        }
        for (int i = 0; i < sortClasses.size();i++) {
            String[] splitedName = sortClasses.get(i).getName().split("\\.");
            String className = splitedName[splitedName.length - 1];
            ex.addRow(className,res[i],method.getName());
            System.out.println(Arrays.toString(res[i]));
        }
    }

}
