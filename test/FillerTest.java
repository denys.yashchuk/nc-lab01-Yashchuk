import org.junit.Test;
import ua.denysyashchuk.nc.Filler;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Tests filler methods
 *
 * @author Yashchuk Denys
 * @author denys.yashchuk@gmail.com
 * @version 1.0
 */
public class FillerTest {
    @Test
    public void sorted() throws Exception {
        int[] actual = Filler.sorted(10);
        int[] expected = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        assertTrue(Arrays.equals(actual, expected));
    }

    @Test(expected = IllegalArgumentException.class)
    public void sortedExc() throws Exception {
        int[] actual = Filler.sorted(-1);
    }

    @Test
    public void sortedLastRand() throws Exception {
        int[] actual = Filler.sortedLastRand(10);
        int[] expected = {0, 1, 2, 3, 4, 5, 6, 7, 8};
        assertTrue(Arrays.equals(Arrays.copyOf(actual, actual.length - 1), expected));
    }

    @Test(expected = IllegalArgumentException.class)
    public void sortedLastRandExc() throws Exception {
        int[] actual = Filler.sortedLastRand(-1);
    }

    @Test
    public void inverseSorted() throws Exception {
        int[] actual = Filler.inverseSorted(10);
        int[] expected = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        assertTrue(Arrays.equals(actual, expected));
    }

    @Test(expected = IllegalArgumentException.class)
    public void inverseSortedExc() throws Exception {
        int[] actual = Filler.inverseSorted(-1);
    }

    @Test
    public void randomSorted() throws Exception {
        int length = 12;
        int[] actual = Filler.randomSorted(length);
        assertTrue(actual.length == length);
    }

    @Test(expected = IllegalArgumentException.class)
    public void randomSortedExc() throws Exception {
        int[] actual = Filler.randomSorted(-1);
    }

}