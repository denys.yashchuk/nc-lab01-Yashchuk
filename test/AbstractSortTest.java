import org.junit.Test;
import ua.denysyashchuk.nc.Filler;
import ua.denysyashchuk.nc.sorts.*;
import ua.denysyashchuk.nc.sorts.bubble.MyDirectBS;
import ua.denysyashchuk.nc.sorts.bubble.MyInverseBS;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Tests sorts
 *
 * @author Yashchuk Denys
 * @author denys.yashchuk@gmail.com
 * @version 1.0
 */
public class AbstractSortTest {

    @Test
    public void sort() throws Exception {
        List<AbstractSort> sorts = new ArrayList<>();
        int[] arr = Filler.inverseSorted(15);
        sorts.add(new MyArraysSort(arr));
        sorts.add(new MyMergeSort(arr));
        sorts.add(new MyQuickSort(arr));
        sorts.add(new MySelectionSort(arr));
        sorts.add(new MyDirectBS(arr));
        sorts.add(new MyInverseBS(arr));
        int[] expected = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
        for (AbstractSort s : sorts) {
            s.sort();
            assertTrue(Arrays.equals(s.getArr(), expected));
        }
    }

    @Test
    public void sortWithTime() throws Exception {
        List<AbstractSort> sorts = new ArrayList<>();
        int[] arr = Filler.inverseSorted(15);
        sorts.add(new MyArraysSort(arr));
        sorts.add(new MyMergeSort(arr));
        sorts.add(new MyQuickSort(arr));
        sorts.add(new MySelectionSort(arr));
        sorts.add(new MyDirectBS(arr));
        sorts.add(new MyInverseBS(arr));
        int[] expected = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
        for (AbstractSort s : sorts) {
            long t = s.sortWithTime();
            assertTrue(Arrays.equals(s.getArr(), expected) && t > 0);
        }
    }

    @Test()
    public void sortConstructor() throws Exception{
        List<AbstractSort> sorts = new ArrayList<>();
        sorts.add(new MyArraysSort(null));
        sorts.add(new MyMergeSort(null));
        sorts.add(new MyQuickSort(null));
        sorts.add(new MySelectionSort(null));
        sorts.add(new MyDirectBS(null));
        sorts.add(new MyInverseBS(null));
        int[] expected = new int[0];
        for (AbstractSort s : sorts) {
            long t = s.sortWithTime();
            assertTrue(Arrays.equals(s.getArr(), expected) && t > 0);
        }
    }

}